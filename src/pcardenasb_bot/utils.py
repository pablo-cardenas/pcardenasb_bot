import pandas as pd
import numpy as np
import requests
from datetime import datetime, timedelta, time, date
from zoneinfo import ZoneInfo
import tempfile
import os
import imgkit
from jinja2 import Environment, PackageLoader, select_autoescape
from markupsafe import Markup
from sqlalchemy import create_engine, select
from sqlalchemy.orm import Session
from sqlalchemy.exc import NoResultFound

from .models import Player, Group


class Photo:

    def __init__(self, df, filename, title, width=480):
        self.tmpdir = tempfile.TemporaryDirectory()
        self.photo_path = os.path.join(self.tmpdir.name, filename)
        self.title = title
        self.df = df
        self.width = width

    def __enter__(self):
        self.tmpdir.__enter__()

        env = Environment(loader=PackageLoader("pcardenasb_bot"),
                          autoescape=select_autoescape())
        table_template = env.get_template('table.html')
        df_html = self.df.to_html(index=False)

        html = table_template.render(title=self.title,
                                     df_html=Markup(df_html),
                                     width=self.width)
        imgkit.from_string(html,
                           '/home/pablo/table.png',
                           options={
                               'quiet': '',
                               'width': str(self.width)
                           })
        imgkit.from_string(html,
                           self.photo_path,
                           options={
                               'quiet': '',
                               'width': str(self.width)
                           })
        return self.photo_path

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.tmpdir.__exit__(exc_type, exc_val, exc_tb)


def get_standing_df(group_id, start, end):
    engine = create_engine('sqlite:///data.db')
    with Session(engine) as session:
        players = session.execute(
            select(Player.user).filter_by(group_id=group_id)).scalars().all()

    if not players:
        return None

    start_timestamp = int(start.timestamp())
    end_timestamp = int(end.timestamp())
    summaries = []
    for player in players:
        url = 'https://data.typeracer.com/games?'\
                f'playerId=tr:{player}&'\
                'universe=play&'\
                f'startDate={start_timestamp}&'\
                f'endDate={end_timestamp}'
        res = requests.get(url)
        if res.status_code == 404:
            summary = {
                'player': player,
                'points': 0,
                'count': 0,
                'wpm': np.nan,
                'acc': np.nan,
            }
        else:
            df = pd.DataFrame(res.json())
            summary = {
                'player': player,
                'points': df['pts'].sum().astype(int),
                'count': len(df.index),
                'wpm': df['wpm'].mean().round(1),
                'acc': df['ac'].mean().round(2),
            }

        summaries.append(summary)

    df = pd.DataFrame(summaries)
    df.sort_values(['points', 'count', 'wpm', 'acc'],
                   ascending=False,
                   inplace=True)
    df.insert(0, '#', range(1, len(df.index) + 1))

    return df
