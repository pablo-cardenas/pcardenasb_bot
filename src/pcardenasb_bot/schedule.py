import os
from jinja2 import Environment, PackageLoader, select_autoescape
from markupsafe import Markup
from zoneinfo import ZoneInfo
from datetime import date, datetime, timedelta, time
import imgkit
import tempfile
import telegram
from sqlalchemy import create_engine, select
from sqlalchemy.orm import Session
from .models import Group
from .utils import get_standing_df, Photo


def day(bot: telegram.Bot) -> None:
    start = datetime.combine(datetime.now() - timedelta(days=0.5),
                             time(),
                             tzinfo=ZoneInfo('America/Lima'))
    end = start + timedelta(days=1)

    engine = create_engine('sqlite:///data.db')
    with Session(engine) as session:
        groups = session.execute(select(Group)).scalars().all()

        for group in groups:
            df = get_standing_df(group.id, start=start, end=end)

            if df is None:
                return

            title = 'Day report {}'.format(start.date().isoformat())
            with Photo(df, 'table.png', title) as photo_path:
                bot.send_photo(group.chat_id,
                               open(photo_path, 'rb'),
                               caption="#typeracer #daily")


def week(bot: telegram.Bot) -> None:
    start = datetime.combine(datetime.now() - timedelta(weeks=0.5),
                             time(),
                             tzinfo=ZoneInfo('America/Lima'))
    start = start - timedelta(days=start.weekday())
    end = start + timedelta(weeks=1)

    engine = create_engine('sqlite:///data.db')
    with Session(engine) as session:
        groups = session.execute(select(Group)).scalars().all()

        for group in groups:
            df = get_standing_df(group.id, start=start, end=end)

            if df is None:
                return

            title = 'Week report {} - {}'.format(
                start.date().isoformat(),
                end.date().isoformat(),
            )
            with Photo(df, 'table.png', title) as photo_path:
                bot.send_photo(group.chat_id,
                               open(photo_path, 'rb'),
                               caption="#typeracer #weekly")
