from datetime import datetime, timedelta, time, date
from zoneinfo import ZoneInfo

import requests
from sqlalchemy import create_engine, select
from sqlalchemy.orm import Session
from sqlalchemy.exc import NoResultFound

from telegram import Update
from telegram.ext import CallbackContext
from .models import Player, Group

from .utils import get_standing_df, Photo


def standing(update: Update, title, start, end) -> None:
    if update.message is None:
        return

    group_chat_id = update.message.chat.id
    engine = create_engine('sqlite:///data.db')
    with Session(engine) as session:
        try:
            group = session.execute(
                select(Group).filter_by(chat_id=group_chat_id)).scalar_one()
        except NoResultFound:
            update.message.reply_text("Bot isn't enabled in this group")
            return

    df = get_standing_df(group.id, start, end)

    if df is None:
        update.message.reply_text('There are no players.')
        return

    with Photo(df, 'table.png', title) as photo_path:
        update.message.reply_photo(open(photo_path, 'rb'))


def day(update: Update, context: CallbackContext) -> None:
    last_noon = datetime.combine(
        date.today(),
        time(),
        tzinfo=ZoneInfo('America/Lima'),
    )
    next_noon = last_noon + timedelta(days=1)

    now = datetime.now(ZoneInfo('America/Lima'))
    title = "Day report up to {}".format(
        now.isoformat(sep=" ", timespec="seconds"))

    standing(update, title, start=last_noon, end=next_noon)


def week(update: Update, context: CallbackContext) -> None:
    last_monday = datetime.combine(
        date.today() - timedelta(days=date.today().weekday()),
        time(),
        tzinfo=ZoneInfo('America/Lima'),
    )
    next_monday = last_monday + timedelta(weeks=1)

    now = datetime.now(ZoneInfo('America/Lima'))
    title = "Week report up to {}".format(
        now.isoformat(sep=" ", timespec="seconds"))
    standing(update, title, start=last_monday, end=next_monday)


def start(update: Update, context: CallbackContext) -> None:
    if update.message is None:
        return

    if update.message.chat.type != 'group':
        update.message.reply_text("this chat is not a group!")
        return

    engine = create_engine('sqlite:///data.db')
    with Session(engine) as session:
        group_chat_id = update.message.chat.id

        try:
            session.execute(
                select(Group).filter_by(chat_id=group_chat_id)).scalar_one()
        except NoResultFound:
            session.add(Group(chat_id=group_chat_id))
            session.commit()
            update.message.reply_text("Bot enabled successfully in this group")
        else:
            update.message.reply_text("Bot is already enabled in this group")


def end(update: Update, context: CallbackContext) -> None:
    if update.message is None:
        return

    if update.message.chat.type != 'group':
        update.message.reply_text("this chat is not a group!")
        return

    engine = create_engine('sqlite:///data.db')
    with Session(engine) as session:
        group_chat_id = update.message.chat.id

        try:
            group = session.execute(
                select(Group).filter_by(chat_id=group_chat_id)).scalar_one()
        except NoResultFound:
            session.add(Group(chat_id=group_chat_id))
            session.commit()
            update.message.reply_text("Bot wasn't enabled in this group")
        else:
            session.delete(group)
            session.commit()
            update.message.reply_text(
                "Bot disabled successfully in this group")


def add_user(update: Update, context: CallbackContext) -> None:
    if update.message is None or context.args is None:
        return

    if update.message.chat.type != 'group':
        update.message.reply_text("this chat is not a group!")
        return

    if len(context.args) != 1:
        update.message.reply_text("Usage: /add <username>")
        return

    engine = create_engine('sqlite:///data.db')
    with Session(engine) as session:
        group_chat_id = update.message.chat.id
        try:
            group = session.execute(
                select(Group).filter_by(chat_id=group_chat_id)).scalar_one()
        except NoResultFound:
            update.message.reply_text(
                "Cannot add users in since bot isn't enabled.")
            return

        user = context.args[0]

        try:
            session.execute(select(Player).filter_by(group=group,
                                                     user=user)).scalar_one()
        except NoResultFound:
            url = f'https://data.typeracer.com/users?id=tr:{user}'
            res = requests.get(url)
            if res.status_code == 404:
                update.message.reply_text(
                    f"User `{user}` not found in typeracer.",
                    parse_mode='markdown')
                return
            else:
                session.add(Player(user=user, group=group))
                session.commit()
                update.message.reply_text(f"User `{user}` added successfully.",
                                          parse_mode='markdown')
        else:
            update.message.reply_text(f"User `{user}` was already added.",
                                      parse_mode='markdown')


def remove_user(update: Update, context: CallbackContext) -> None:
    if update.message is None or context.args is None:
        return

    if update.message.chat.type != 'group':
        update.message.reply_text("this chat is not a group!")
        return

    if len(context.args) != 1:
        update.message.reply_text("Usage: /remove_bot <username>")
        return

    engine = create_engine('sqlite:///data.db')
    with Session(engine) as session:
        group_chat_id = update.message.chat.id
        try:
            group = session.execute(
                select(Group).filter_by(chat_id=group_chat_id)).scalar_one()
        except NoResultFound:
            update.message.reply_text(
                "Cannot remove users in since bot isn't enabled.")
            return

        user = context.args[0]

        try:
            player = session.execute(
                select(Player).filter_by(group=group, user=user)).scalar_one()
        except NoResultFound:
            update.message.reply_text(f"User `{user}` wan't added",
                                      parse_mode='markdown')
        else:
            session.delete(player)
            session.commit()
            update.message.reply_text(f"User `{user}` removed successfully.",
                                      parse_mode='markdown')
