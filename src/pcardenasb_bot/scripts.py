import click
import os
from .models import Base
from sqlalchemy import create_engine
from telegram.ext import Updater, CommandHandler
import telegram
from . import bot, schedule

TOKEN = '5202773120:AAGSd-if1ktDT3_yHTmoXjUkxnWDbttMkEk'


@click.group()
def cli() -> None:
    pass


@cli.command()
def init_db():
    engine = create_engine('sqlite:///data.db', echo=True)
    Base.metadata.create_all(engine)


@cli.command()
def start():
    token = os.getenv('TELEGRAM_TOKEN')
    updater = Updater(token)

    updater.dispatcher.add_handler(CommandHandler('day', bot.day))
    updater.dispatcher.add_handler(CommandHandler('week', bot.week))
    updater.dispatcher.add_handler(CommandHandler('start', bot.start))
    updater.dispatcher.add_handler(CommandHandler('end', bot.end))
    updater.dispatcher.add_handler(CommandHandler('add', bot.add_user))
    updater.dispatcher.add_handler(CommandHandler('remove', bot.remove_user))

    updater.start_polling()
    updater.idle()


@cli.command()
def send_daily():
    token = os.getenv('TELEGRAM_TOKEN')
    bot = telegram.Bot(token)
    schedule.day(bot)


@cli.command()
def send_weekly():
    token = os.getenv('TELEGRAM_TOKEN')
    bot = telegram.Bot(token)
    schedule.week(bot)
