from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import declarative_base, relationship

Base = declarative_base()


class Player(Base):
    __tablename__ = 'player'
    id = Column(Integer, primary_key=True)
    user = Column(String)
    group_id = Column(Integer, ForeignKey('group.id', ondelete="CASCADE"), nullable=False)

    group = relationship('Group', back_populates='players')


class Group(Base):
    __tablename__ = 'group'
    id = Column(Integer, primary_key=True)
    chat_id = Column(Integer, nullable=False, unique=True)

    players = relationship('Player', back_populates='group', cascade='all, delete-orphan')


if __name__ == '__main__':
    from sqlalchemy import create_engine
    engine = create_engine('sqlite:///data.db', echo=True)
    Base.metadata.create_all(engine)
